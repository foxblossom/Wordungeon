extends Node

@export var display_name = "Artifact"
@export var image : Texture2D
@export var description = ""
@export var maximum_held = 1

@onready var character = $/root/Character

var number_held = 1

# Effect for an artifact when it is acquired
func on_pickup():
	pass

# Effect for the artifact when a word's value has been calculated
func on_calculate_word(word, score):
	return score
	
# Efect for the artifact when the word is executed (i.e. when strength and damage mods have been applied)
func on_attack(word, damage):
	return damage
	
# Effect when the player takes damage
func on_player_hit(damage):
	return damage
	
# Effect when a battle begins
func on_battle_start():
	pass
	
# Effect when a battle ends
func on_battle_end():
	pass
