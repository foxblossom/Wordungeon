extends Node

var max_hp = 100
var hp = 100
var strength = 10
var agility = 10

var level = 1
var experience = 0

@onready var artifact_manager = $Artifacts

# Initialise a new character at the start of a run
func initialise():
	max_hp = 100
	hp = 100
	strength = 10
	agility = 10
	level = 1
	experience = 0
	
	artifact_manager.clear()
	artifact_manager.add_artifact("StrengthBoost")
