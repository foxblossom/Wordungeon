extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	%FloorNumber.text = str($"/root/RunManager".current_floor)
	%GoldLabel.text = str($"/root/RunManager".gold)

func _on_button_pressed():
	get_tree().change_scene_to_file("res://Battle/Battle.tscn")
