extends Node2D

func add_artifact(artifact_name):
	var prefab = load("res://Run/Artifacts/" + artifact_name + ".tscn")
	
	if !prefab:
		print("ERROR: Artifact could not be loaded - " + artifact_name)
		return
	
	var artifact = prefab.instantiate()
	add_child(artifact)
	
	artifact.on_pickup()
	
func clear():
	for child in get_children():
		remove_child(child)
		child.queue_free()
	
func on_battle_start():
	for child in get_children():
		child.on_battle_start()
		
func on_battle_end():
	for child in get_children():
		child.on_battle_end()
		
func on_take_damage(damage):
	for child in get_children():
		damage = child.on_player_hit(damage)
		
	return damage

# Effect for the artifact when a word's value has been calculated
func on_calculate_word(word, score):
	for child in get_children():
		score = child.on_calculate_word(word, score)
	
	return score
	
# Efect for the artifact when the word is executed (i.e. when strength and damage mods have been applied)
func on_attack(word, damage):
	for child in get_children():
		damage = child.on_calculate_word(word, damage)

	return damage
