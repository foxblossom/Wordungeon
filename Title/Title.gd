extends Node2D

func _on_new_run_pressed():
	$"/root/RunManager".start_run()
	$"/root/Character".initialise()
	get_tree().change_scene_to_file("res://Run/NextStage.tscn")
