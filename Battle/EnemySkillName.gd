extends Control

@onready var skill_label = $PanelContainer/MarginContainer/EnemySkillName
@onready var timer = $EnemySkillTimer

func run(skill_name):
	skill_label.text = skill_name
	timer.start()
	show()
	
func finish():
	hide()
