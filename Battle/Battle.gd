extends Node2D

enum EBattleState {
	Active,
	Victory,
	Defeat,
	Paused
}

var state = EBattleState.Active
var victory_timer = 0
var victory_timer_max = 3

var enemy = null

@onready var battle_ui = $CanvasLayer/BattleUI
@onready var player = %BattlePlayer
@onready var enemy_sprite = %EnemySprite

# Called when the node enters the scene tree for the first time.
func _ready():
	var prefab = load("res://Battle/Enemies/Test.tscn")
	enemy = prefab.instantiate()
	
	enemy.connect("HpChanged", battle_ui._on_battle_enemy_hp_changed)
	enemy.connect("SetHP", battle_ui._on_battle_enemy_set_hp)
	enemy.connect("SetName", battle_ui._on_battle_enemy_set_name)
	enemy.connect("ExecuteAttack", battle_ui._on_battle_enemy_execute_attack)
	enemy.connect("OnDeath", _on_victory)
	enemy.connect("HpChanged", %EnemyDamageSpawner.spawn)
	
	player.connect("AttackEnemy", enemy.damage)
	enemy.connect("AttackPlayer", player.damage)	

	enemy_sprite.texture = enemy.enemy_sprite
	enemy_sprite.add_child(enemy)
	
	$/root/Character.artifact_manager.on_battle_start()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if state == EBattleState.Victory:
		victory_timer += delta
		if victory_timer >= victory_timer_max:
			$"/root/RunManager".gold += enemy.gold
			$"/root/RunManager".next_floor()
			get_tree().change_scene_to_file("res://Run/NextStage.tscn")
			
	if state == EBattleState.Defeat:
		victory_timer += delta
		if victory_timer >= victory_timer_max:
			get_tree().change_scene_to_file("res://Title/Title.tscn")

# Check whether the battle is running.
func is_active():
	return state == EBattleState.Active

# Run gameover scripts
func _on_gameover():
	state = EBattleState.Defeat
	battle_ui.defeat()
	
# Run victory scripts.
func _on_victory():
	enemy_sprite.self_modulate = Color.TRANSPARENT
	state = EBattleState.Victory
	battle_ui.victory()
	
	$/root/Character.artifact_manager.on_battle_end()
