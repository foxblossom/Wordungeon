extends Node2D

@export_category("Base Stats")
@export var enemy_name = "Enemy Name"
@export var enemy_sprite: Texture2D
@export var max_hp = 100
@export var strength = 10
@export var agility = 20

@export_category("Rewards")
@export var experience = 10
@export var gold = 10

signal SetHP(value)
signal HpChanged(change, new_value)
signal SetName(enemy_name)
signal AttackPlayer(damage)
signal OnDeath
signal ExecuteAttack

var hp = 100
var attack_timer = 0
var attack_timer_max = 100

# Force the UI to update based on existing data
func _ready():
	hp = max_hp
	emit_signal("SetHP", hp)
	emit_signal("SetName", enemy_name)

func _process(delta):
	if hp > 0 and $/root/Battle.is_active():
		attack_timer += delta * get_speed()
		
		if attack_timer >= attack_timer_max:
			attack_timer -= attack_timer_max
			attack()

# Speed is calculated by the enemy's speed relative to the character's
func get_speed():
	return int(float(agility / $"/root/Character".agility) * 10)

# Heal the enemy
func heal(value):
	update_hp(value)

# Hurt the enemy
func damage(value):
	update_hp(-value)
	
# Change the enemy's HP and let any subscribed events know
func update_hp(value):
	hp += value
	
	if hp <= 0:
		hp = 0
		die()
	
	if hp > max_hp:
		hp = max_hp
	
	emit_signal("HpChanged", value, hp)
		
# Perform an attack
func attack():
	emit_signal("ExecuteAttack", "Attack")
	emit_signal("AttackPlayer", strength)

func die():
	emit_signal("OnDeath")
