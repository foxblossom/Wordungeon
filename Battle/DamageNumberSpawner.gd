extends Node2D

var prefab = preload("res://Battle/DamageLabel.tscn")

func spawn(change, _new_value):
	var number = prefab.instantiate()
	add_child(number)
	
	number.set_value(change)
