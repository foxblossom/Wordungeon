extends Control

const LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

var letter = 'A'
var clicked = false
var letter_value = 0

signal Letter_Selected(letter)

@onready var letter_label = $LetterLabel
@onready var score_label = %ScoreLabel

# Called when the node enters the scene tree for the first time.
func _ready():
	set_letter()

# Select a random letter for this cell
func set_letter():
	var rand = $/root/WordManager.get_random_letter()
	
	letter = rand.letter
	letter_value = rand.score
	letter_label.text = letter
	score_label.text = str(letter_value)
	
# Set a new letter, and make this button selectable again.
func regenerate(_submitted_word, _score):
	if clicked:
		clear()
		set_letter()

# Button is pressed - add the letter to the word, if it hasn't already been.
func _on_pressed():
	if !clicked and $/root/Battle.is_active():
		clicked = true
		emit_signal("Letter_Selected", letter)
		letter_label.modulate = Color.GOLD
		
# Clear the box & reset the Active state
func clear():
	clicked = false
	letter_label.modulate = Color.WHITE
