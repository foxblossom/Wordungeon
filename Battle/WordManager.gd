extends Node
enum LETTER_RARITIES  {
	Core,
	Common,
	Uncommon,
	Rare
}

const LETTER_DICT = {
	LETTER_RARITIES.Core: {
		"value": 1,
		"letters": ["A", "E", "R", "S", "T"]
	},
	LETTER_RARITIES.Common: {
		"value": 2,	
		"letters": ["I", "O", "U", "B", "C", "D", "H", "L", "M", "N", "P"]
	},
	LETTER_RARITIES.Uncommon: {
		"value": 3,
		"letters": ["F", "G", "K", "V", "W", "Y"]
	},
	LETTER_RARITIES.Rare: {
		"value": 5,
		"letters": ["Z", "X", "Q", "J"]
	}
}

var rng = RandomNumberGenerator.new()

var current_word = ""
var current_score = 0

# Add a letter to the currently active word
func add_letter(letter):
	current_word = current_word + letter
	calculate_score()
	
# Clear the currently active word & reset score
func clear_word():
	current_word = ""
	current_score = 0

# Get a random letter
func get_random_letter():
	# First pass: Select common / uncommon / rare
	var rarity = rng.randi_range(0, 100)
	var rarity_tag = ""
	
	if (rarity > 97):
		rarity_tag = LETTER_RARITIES.Rare
	elif (rarity > 80):
		rarity_tag = LETTER_RARITIES.Uncommon
	elif (rarity > 45):
		rarity_tag = LETTER_RARITIES.Common
	else:
		rarity_tag = LETTER_RARITIES.Core
	
	var letters = LETTER_DICT[rarity_tag]["letters"]
	
	#Second pass - retrieve letter from the given array
	var letter = rng.randi_range(0, len(letters) - 1)
	
	return {
		"letter": letters[letter],
		"score": LETTER_DICT[rarity_tag].value
	}

# Check if the current word is valid
func valid_word():
	if (len(current_word) < 4):
		return false
		
	return $/root/Wordlist.check_word(current_word)

# TODO Optimise later
func get_letter_value(letter):
	for rarity in LETTER_DICT:
		if LETTER_DICT[rarity].letters.has(letter):
			return LETTER_DICT[rarity].value
			
	return 1

# Calculate the score of the current word
func calculate_score():
	var score = 0
	for c in current_word:
		score += get_letter_value(c)
				
	# Bonus for every letter after the first 4
	var bonus_letters = len(current_word) - 4
	var base_bonus_multiplier = 1.1
	if bonus_letters > 0:
		var bonus = pow(base_bonus_multiplier, bonus_letters)
		score *= bonus
	
	score = $/root/Character.artifact_manager.on_calculate_word(current_word, score)
	
	current_score = int(score)
