extends Node

var wordlist = []

func _ready():
	var path = "res://wordlist.txt"
	var file = FileAccess.open(path, FileAccess.READ)

	while not file.eof_reached():
		var line = file.get_line()
		if len(line) < 4:
			continue
		
		wordlist.push_back(line)
		
	file.close()
	
	print("Loaded word list")
	
func check_word(word):
	return wordlist.has(word)
