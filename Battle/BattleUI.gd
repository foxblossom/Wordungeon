extends Control

signal ConfirmWord(word, score)

@onready var enemy_skill_box = %EnemySkillPivot
@onready var player_hp = %PlayerHP
@onready var player_hp_label = %PlayerHP/Label
@onready var enemy_hp = %EnemyHP

func victory():
	$VictoryContainer.show()
	
func defeat():
	$DefeatContainer.show()

# Confirm a word - and send it to non-UI components
func _on_letterr_grid_base_confirm_word(word, score):
	emit_signal("ConfirmWord", word, score)

### PLAYER UI
# Update HP bar when player's HP changes
func _on_battle_player_hp_changed(_change, new_value):
	var max_hp = $"/root/Character".max_hp
	
	player_hp.value = new_value
	player_hp_label.text = str(new_value) + " / " + str(max_hp)
	
# Set initial HP for the player HP bar
func _on_battle_player_set_hp(value, max_value):	
	player_hp.value = value
	player_hp.max_value = max_value
	player_hp_label.text = str(value) + " / " + str(max_value)
	
### ENEMY UI
# Add the enemy name to their HP bar
func _on_battle_enemy_set_name(enemy_name):
	%EnemyName.text = enemy_name
	
# Update Enemy HP bar
func _on_battle_enemy_hp_changed(_change, new_value):
	enemy_hp.value = new_value

func _on_battle_enemy_set_hp(value):
	enemy_hp.value = value
	enemy_hp.max_value = value

func _on_battle_enemy_execute_attack(skill_name):
	enemy_skill_box.run(skill_name)
