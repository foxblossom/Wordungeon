extends Node2D

@onready var label = $Label
@onready var timer = $Timer
@onready var animation = $AnimationPlayer

func set_value(value):
	if value < 0:
		value *= -1
	else:
		label.modulate = Color.GREEN
	
	label.text = str(value)
	
	timer.start()
	animation.play("damage_bounce")

func _on_timer_timeout():
	queue_free()
