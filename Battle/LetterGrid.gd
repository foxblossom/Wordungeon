extends Control

var cell_prefab = preload("res://Battle/LetterCell.tscn")

signal LetterSelected(letter)
signal ConfirmWord(word, score)
signal CancelInput

var current_word = ""

@onready var grid = %LetterGrid
@onready var input_string = %CurrentInputString
@onready var input_score = %CurrentInputScore
@onready var word_manager = $"/root/WordManager"

# Called when the node enters the scene tree for the first time.
func _ready():
	generate()

# Generate a randomized grid of letters
func generate():
	for i in range(0, 16):
		var cell = cell_prefab.instantiate()
		grid.add_child(cell)
		cell.connect("Letter_Selected", _on_letter_cell_clicked)
		connect("CancelInput", cell.clear)
		connect("ConfirmWord", cell.regenerate)
		
func regenerate():
	print("Hold complete!")
	for child in grid.get_children():
		continue

# Get rid of the current word
func cancel_word():
	emit_signal("CancelInput")
	input_string.text = ""
	input_score.text = ""
	word_manager.clear_word()
		
# Single letter pressed
func _on_letter_cell_clicked(letter):
	word_manager.add_letter(letter)
	
	input_score.text = str(word_manager.current_score)
	input_string.text = word_manager.current_word
	
	emit_signal("LetterSelected", letter)

# Word submitted
# Check validity and then either submit or cancel
func _on_submit_word_button_pressed():
	if $/root/Battle.is_active():
		if word_manager.valid_word():
			emit_signal("ConfirmWord", word_manager.current_word, word_manager.current_score)
			input_string.text = ""
			input_score.text = ""
			word_manager.clear_word()
		else:
			cancel_word()

# Cancel pressed - delete the current word
func _on_cancel_word_button_pressed():
	cancel_word()
