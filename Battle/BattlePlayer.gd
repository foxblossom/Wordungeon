extends Node2D

signal SetHP(value, max)
signal HpChanged(change, new_value)
signal GameOver
signal AttackEnemy(damage)

@onready var character = $"/root/Character"

func _ready():
	emit_signal("SetHP", character.hp, character.max_hp)

# Heal the player
func heal(value):
	update_hp(value)

# Hurt the player
func damage(value):
	value = $/root/Character.artifact_manager.on_take_damage(value)

	update_hp(-value)
		
# Change the player's HP and let any subscribed events know
func update_hp(value):
	character.hp += value
	
	if character.hp <= 0:
		character.hp = 0
		game_over()
	
	if character.hp > character.max_hp:
		character.hp = character.max_hp
		
	emit_signal("HpChanged", value, character.hp)
		
# Gameover the player at 0 HP.
func game_over():
	emit_signal("GameOver")

func _on_battle_ui_confirm_word(_word, score): 
	var dmg = score * (character.strength / 10)
	
	dmg = $/root/Character.artifact_manager.on_attack(_word, dmg)

	emit_signal("AttackEnemy", dmg)
