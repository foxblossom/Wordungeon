extends Button

@export var seconds = 2

var timer = 0
var holding = false
var hold_complete = false

signal ButtonHoldClick


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if holding:
		timer += delta
		
		if timer > seconds and !hold_complete:
			hold_complete = true
			emit_signal("ButtonHoldClick")

func start_hold():
	timer = 0
	holding = true

func stop_hold():
	hold_complete = false
	holding = false
